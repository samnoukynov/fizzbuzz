package com.example.demo;

import com.example.demo.service.FizzBuzzService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.ArrayList;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	private FizzBuzzService fizzBuzzService ;

	@Test
	void contextLoads() {
	}

	@Test
	void testReplaceFuzzBizz(){

		ArrayList<String> strings = fizzBuzzService.replaceFuzzBizz(2,3,10,"fizz","buzz");
		Assertions.assertEquals(strings.get(0),"1");
		Assertions.assertEquals(strings.get(1),"fizz");
		Assertions.assertEquals(strings.get(2),"buzz");
		Assertions.assertEquals(strings.get(3),"fizz");
		Assertions.assertEquals(strings.get(4),"5");
		Assertions.assertEquals(strings.get(5),"fizzbuzz");
		Assertions.assertEquals(strings.get(6),"7");
		Assertions.assertEquals(strings.get(7),"fizz");
		Assertions.assertEquals(strings.get(8),"buzz");
		Assertions.assertEquals(strings.get(9),"fizz");
	}

}
