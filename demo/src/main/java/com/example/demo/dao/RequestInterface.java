package com.example.demo.dao;


import com.example.demo.model.RequestFizzBuzz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface RequestInterface extends JpaRepository<RequestFizzBuzz, Long> {

    @Query(value = "SELECT  int1, int2, limit_int, str1, str2 , same\n" +
            "FROM (" +
            "SELECT int1, int2, limit_int, str1, str2 , count(*) AS same FROM request_fizz_buzz GROUP BY int1, int2, limit_int, str1, str2  ORDER BY same DESC )\n" +
            "WHERE same = (SELECT TOP 1 count(*)  FROM request_fizz_buzz GROUP BY int1, int2, limit_int, str1, str2 ORDER BY count(*) DESC)\n" +
            "GROUP BY int1, int2, limit_int, str1, str2 ;", nativeQuery = true)
    List<String> findTheMostUseRequest();


}
