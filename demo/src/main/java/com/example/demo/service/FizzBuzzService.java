package com.example.demo.service;

import com.example.demo.dao.RequestInterface;
import com.example.demo.model.RequestFizzBuzz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FizzBuzzService {

    @Autowired
    private RequestInterface repositoryRequest;

    public ArrayList<String> replaceFuzzBizz( Integer int1,  Integer int2,  Integer limit,
                                              String str1,  String str2 ){

        RequestFizzBuzz requestFizzBuzz = new RequestFizzBuzz(int1,int2,limit,str1,str2);
        repositoryRequest.save(requestFizzBuzz);
        ArrayList<String> arrayString = new ArrayList();
        boolean fini  ;

        for(int i = 1 ; i<= limit; i++ ){

            fini = false ;

            if ( i % int1 == 0 && i % int2 == 0){
                arrayString.add(str1 + str2);
                fini = true ;
            }
            else if ( fini == false && i % int1 == 0 ){

                arrayString.add(str1);
                fini = true ;

            }else if( fini == false && i % int2 == 0){
                arrayString.add(str2);
                fini = true ;
            }
            if(fini == false) {
                arrayString.add(String.valueOf(i));
            }

        }

        return arrayString ;
    }

    public String frequentRequest() {

        List<String> mostRequestArray = repositoryRequest.findTheMostUseRequest() ;
        StringBuilder requestEgality = new StringBuilder();

            for (String string : mostRequestArray){
                String[] Strings = string.split(",");
                requestEgality.append(" //int1:" + Strings[0] + " int2:" +  Strings[1]  + " limit:" +  Strings[2]  +
                        " str1:" +  Strings[3]  + " str2:" +  Strings[4]  + " number of uses:" +  Strings[5] );
            }

            return requestEgality.toString() ;

    }

}
