package com.example.demo.model;


import javax.persistence.*;

@Entity
public class RequestFizzBuzz {

    @Id
    @Column( unique = true, nullable = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_Sequence")
    @SequenceGenerator(name = "id_Sequence", sequenceName = "ORACLE_DB_SEQ_ID")
    private Long id ;

    private Integer int1;
    private Integer int2;
    private Integer limitInt;
    private String str1;
    private String str2;

    public RequestFizzBuzz(){}

    public RequestFizzBuzz(Integer int1, Integer int2, Integer limitInt, String str1, String str2) {
        this.int1 = int1;
        this.int2 = int2;
        this.limitInt = limitInt;
        this.str1 = str1;
        this.str2 = str2;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setInt1(Integer int1) {
        this.int1 = int1;
    }

    public void setInt2(Integer int2) {
        this.int2 = int2;
    }

    public void setLimitInt(Integer limitInt) {
        this.limitInt = limitInt;
    }

    public void setStr1(String str1) {
        this.str1 = str1;
    }

    public void setStr2(String str2) {
        this.str2 = str2;
    }
}
