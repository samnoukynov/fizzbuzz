package com.example.demo.controller;


import com.example.demo.model.RequestFizzBuzz;
import com.example.demo.service.FizzBuzzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.Http2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Controller
public class FizzBuzzController {


    @Autowired
    private FizzBuzzService fizzBuzzService ;


    @GetMapping("/fizzbuzz")
    public ResponseEntity<ArrayList<String>> fizzBuzz(@RequestParam Integer int1, @RequestParam Integer int2, @RequestParam Integer limit,
                                                     @RequestParam String str1, @RequestParam String str2 ){

        return new ResponseEntity(fizzBuzzService.replaceFuzzBizz(int1,int2,limit,str1,str2 ),HttpStatus.OK) ;
    }

    @GetMapping("/frequentRequest")
    public ResponseEntity<String> frequentRequest(){

        return new ResponseEntity( fizzBuzzService.frequentRequest() ,HttpStatus.OK) ;
    }

}
